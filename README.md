# Working Group Community Survey

This group is about designing a community survey, executing it and analysing / publishing results.

- `@dominikhaas:matrix.org` started to design a survey
- Possible topics:
    - Expectations from community
    - Work situation:
        - Research aspects
        - Techincal aspects
        - Personal situation 
    - Also survey for under represented groups (gender, ethnics, mental issues, disabilities)
- Check https://softwaresaved.github.io/international-survey-2022/ for ideas
- There might be other RSE surveys

